# C-assignment



## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/michaelrusezera/c-assignment.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/michaelrusezera/c-assignment/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Set auto-merge](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing (SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***
## Name
STUDENT'S DATA MANAGEMENT SYSTEM

## Description
The provided C program is a simple student records management system. It allows users to perform various operations on student records, including creating, reading, updating, and deleting records. Users can also search for students by registration number and sort student records by name or registration number. Additionally, the program provides functionality to export student records to a CSV file. The user interacts with the system through a text-based menu interface, making it straightforward to use and navigate. Overall, the program offers basic yet essential functionalities for managing student information efficiently.

## Consirations for the data structure
In this student records management system, the main data structure used is a struct called `Student`. This struct contains fields for various pieces of information about each student, such as their name, date of birth, registration number, program code, and tuition. 

When selecting this data structure, several considerations were likely taken into account:

1.Fields Needed: The data structure should include fields to store all relevant information about a student. In this case, the fields chosen cover common attributes of a student record, such as personal information, academic details, and financial information.

2.Memory Efficiency: The data structure should use memory efficiently while still providing all necessary information. Using a struct allows for grouping related data together in a single entity, which can improve memory locality and access efficiency.

3.Ease of Access and Manipulation: The chosen data structure should allow for easy access and manipulation of student records. With a struct, individual fields can be accessed using dot notation, making it intuitive to work with and modify student information.

4.Flexibility: The data structure should be flexible enough to accommodate changes or additions to student record fields in the future. By defining a struct with named fields, it's easier to add or modify fields as needed without affecting the overall structure of the program.

5. Array for Multiple Students: Since the program needs to manage multiple student records, an array of `Student` structs (`students[MAX_STUDENTS]`) is used to store these records. This allows for efficient storage and retrieval of student information, with the ability to handle up to a maximum number of students defined by `MAX_STUDENTS`.
## links to youtube videos of group mrmbers explaining their parts
https://youtu.be/2K2gvhhdZkc?si=VWdSsSO85yWna9cO
https://youtu.be/PAu76E8Ufm0?si=J97Qa1_tFVED_9hJ
https://youtu.be/sLd7oNe9XO4?si=Dr_8UBC8wWQiO7HV
https://youtu.be/W8ZVeTEFEg8?si=l0ES9N1318V97oYA
https://youtu.be/TJ1jQGbmtUw


#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_STUDENTS 100
#define MAX_NAME_LENGTH 51 // 50 characters + '\0'
#define DATE_LENGTH 11 // 10 characters (YYYY-MM-DD) + '\0'
#define REGISTRATION_LENGTH 7 // 6 digits + '\0'
#define PROGRAM_CODE_LENGTH 5 // 4 characters + '\0'

// Define a struct to represent a student
struct Student {
    char name[MAX_NAME_LENGTH]; // Student's name
    char dob[DATE_LENGTH]; // Student's date of birth (YYYY-MM-DD)
    char registration[REGISTRATION_LENGTH]; // Student's registration number
    char program_code[PROGRAM_CODE_LENGTH]; // Student's program code
    float tuition; // Student's tuition fee
};

// Initialize an array of Student structs and a variable to keep track of the number of students
struct Student students[MAX_STUDENTS];
int num_students = 0;

// Function prototypes
void createStudent(); //creates a new student record
void readStudent(); //reads and prints the details for all students records
void updateStudent(); //updates details of existing student records
void deleteStudent(); //deletes an existing student record
void searchStudent(); //searches for a student record
void sortStudentsByName();//sorts students by name
void sortStudentsByRegistration();//sorts students by registration numbers
void exportRecords();//export the student records to a CSV file
int main() {
    int choice; // variable to store the user's choice

//Main menu loop
    do {
        //Print the menu options
        printf("\nMenu:\n");
        printf("1. Create Student\n"); // option 1: create a new student record
        printf("2. Read Student\n"); // option 2: read and print all student records
        printf("3. Update Student\n"); // option 3: update an existing student record
        printf("4. Delete Student\n"); //option 4: delete an existing student record
        printf("5. Search Student\n"); //allows a user to search for a specific student detail
        printf("6. Sort Students by Name\n");//enables a user to sort students by their names
        printf("7. Sort Students by Registration\n");// enables a user to sort students by their registration numbers
        printf("8. Export Records\n");//allows the user to export the student records to a CSV file
        printf("0. Exit\n");//terminates the program.
        printf("Enter your choice: "); // prompt the user to enter their choice
        scanf("%d", &choice); // read the user's choice into the 'choice' variable
              
//Switch case to handle user's choice
        switch(choice) {
            case 1:
                createStudent(); // call the createStudent() function
                break;
            case 2:
                readStudent(); // call the readStudent() function
                break;
            case 3:
                updateStudent(); // call the updateStudent() function
                break;
            case 4:
                deleteStudent(); // call the deleteStudent() function
                break;
            case 5:
                searchStudent();// call the searchStudent() function
                break;
            case 6:
                sortStudentsByName();// call the sortStudentsByName() function
                break;
            case 7:
                sortStudentsByRegistration();// call the sortStudentsByRegistration() function
                break;
               case 8:
                exportRecords();//call the export records() function
                break;
            case 0:
                printf("Exiting...\n");
                break;
            default:
                printf("Invalid choice! Please try again.\n"); 
        }
    } while(choice != 0); // loop until the user chooses to exit (choice == 0)

    return 0;
}

//Function to create a new student record
void createStudent() {
    if (num_students >= MAX_STUDENTS) {
        printf("Maximum number of students reached!\n");
        return;
    }
//create a new student record
    struct Student new_student;

// Prompt the user to enter the student's name
    printf("Enter student name: ");
    scanf(" %[^\n]", new_student.name);

  // Prompt the user to enter the student's date of birth
    printf("Enter student date of birth (YYYY-MM-DD): ");
    scanf("%s", new_student.dob);

    // Prompt the user to enter the student's registration number
    printf("Enter student registration number: ");
    scanf("%s", new_student.registration);

    // Prompt the user to enter the student's program code
    printf("Enter student program code: ");
    scanf("%s", new_student.program_code);

      // Prompt the user to enter the student's annual tuition
    printf("Enter student annual tuition: ");
    scanf("%f", &new_student.tuition);

    // Add the new student record to the array of students
    students[num_students++] = new_student;
   
    printf("Student added successfully!\n");
}

//Function to read and display all student records
void readStudent() {
    if (num_students == 0) {
        printf("No students found!\n");
        return;
    }
//implementation
    printf("List of students:\n");
    for (int i = 0; i < num_students; i++) {
        printf("Student %d:\n", i + 1);
        printf("Name: %s\n", students[i].name);
        printf("Date of Birth: %s\n", students[i].dob);
        printf("Registration Number: %s\n", students[i].registration);
        printf("Program Code: %s\n", students[i].program_code);
        printf("Annual Tuition: %.2f\n", students[i].tuition);
    }
}

//Function to update an existing student record
void updateStudent() {
    // Check if there are any students in the array
    if (num_students == 0) {
        printf("No students found!\n");
        return;
    }
// Prompt the user to enter the registration number of the student to update
    char reg_num[REGISTRATION_LENGTH];
    printf("Enter student registration number to update: ");
    scanf("%s", reg_num);

// Initialize a flag to indicate whether the student was found
    int found = 0;

 // Loop through the array of students
    for (int i = 0; i < num_students; i++) {
        // Check if the current student's registration number matches the one entered by the use 
        if (strcmp(students[i].registration, reg_num) == 0) {
           // Prompt the user to enter the new student name
            printf("Enter new student name: ");
            scanf(" %[^\n]", students[i].name);

            // Prompt the user to enter the new student date of birth
            printf("Enter new student date of birth (YYYY-MM-DD): ");
            scanf("%s", students[i].dob);

            // Prompt the user to enter the new student program code
            printf("Enter new student program code: ");
            scanf("%s", students[i].program_code);

            // Prompt the user to enter the new student annual tuition
            printf("Enter new student annual tuition: ");
            scanf("%f", &students[i].tuition);

            printf("Student updated successfully!\n");
            found = 1;
            break;
        }
    }
    // Check if the student was not found
    if (!found) {
        printf("Student with registration number %s not found!\n", reg_num);
    }
}

//Function to delete an existing student record
void deleteStudent() {
    //Check if there are any students in the array
    if (num_students == 0) {
        printf("No students found!\n");
        return;
    }
//implementation
    char reg_num[REGISTRATION_LENGTH];
    printf("Enter student registration number to delete: ");
    scanf("%s", reg_num);

    int found = 0;

    // Loop through the array of students
    for (int i = 0; i < num_students; i++) {
        // Check if the current student's registration number matches the one entered by the user
        if (strcmp(students[i].registration, reg_num) == 0) {
             // Shift all students after the current one down by one position
            for (int j = i; j < num_students - 1; j++) {
                students[j] = students[j + 1];
            }

             // Decrement the number of students
            num_students--;
            printf("Student deleted successfully!\n");
            found = 1;
            break;
        }
    }
//Check if the student was not found
    if (!found) {
        printf("Student with registration number %s not found!\n", reg_num);
    }
}

//Function to search for a student record by registration number
void searchStudent() {
    if (num_students == 0) { //check if there are any students in the array
        printf("No students found!\n");
        return;
    }
//implementation
    char reg_num[REGISTRATION_LENGTH]; //create a character array to store the registration number
    printf("Enter student registration number to search: ");
    scanf("%s", reg_num); //prompt the user to enter the registration number and scan it into the reg_num array

    int found = 0; //initialize a flag variable to keep track of whether the student is found or not
    for (int i = 0; i < num_students; i++) { //loop through the array of students
        if (strcmp(students[i].registration, reg_num) == 0) { //compare the registration number of each student to the one entered by the user
            printf("Student found:\n");
            printf("Name: %s\n", students[i].name); //if a match is found, print the details of the student
            printf("Date of Birth: %s\n", students[i].dob);
            printf("Program Code: %s\n", students[i].program_code);
            printf("Annual Tuition: %.2f\n", students[i].tuition);
            found = 1; //set the flag variable to 1
            break; //break out of the loop to avoid unnecessary comparisons
        }
    }

    if (!found) { //if the flag variable is still 0 after the loop, the student was not found
        printf("Student with registration number %s not found!\n", reg_num);
    }
}

void sortStudentsByName() {//function definition: it takes in no arguements and returns void
    if (num_students == 0) {// checks if there is no student in the array
        printf("No students found!\n");// message to be printed if no students are found
        return;
    }

    // Bubble sort implementation for simplicity
    for (int i = 0; i < num_students - 1; i++) {//outer loop for bubble sort
        for (int j = 0; j < num_students - i - 1; j++) {//inner loop for bubble sort
            if (strcmp(students[j].name, students[j + 1].name) > 0) {// compares the student names
                struct Student temp = students[j];//temporary variable that stores student during swapping
                students[j] = students[j + 1];//swaps students if there in the wrong order
                students[j + 1] = temp;// Completes the swap operation
            }
        }
    }

    printf("Students sorted by name:\n");// messege printed before the list
    readStudent(); // Print the sorted list
}

void sortStudentsByRegistration() {//function definition: it takes in no arguements and returns void
    if (num_students == 0) {// checks if there is no student in the array
        printf("No students found!\n");// message to be printed if no students are found
        return;
    }

    // Bubble sort implementation for simplicity
    for (int i = 0; i < num_students - 1; i++) {//outer loop for buble sort
        for (int j = 0; j < num_students - i - 1; j++) {//inner loop for bubble sort
            if (strcmp(students[j].registration, students[j + 1].registration) > 0) {// compares the student registratiion numbers
                struct Student temp = students[j];//temporary variable that stores student during swapping
                students[j] = students[j + 1];//swaps students if there in the wrong order
                students[j + 1] = temp;// Completes the swap operation
            }
        }
    }

    printf("Students sorted by registration number:\n");// messege printed before the list
    readStudent(); // Print the sorted list
}

// Function to export student records to a CSV file
void exportRecords() {
    // Open the file "student_records.csv" in append mode
    FILE *fp = fopen("student_records.csv", "a");

    // Check if the file was opened successfully
    if (fp == NULL) {
        // If the file cannot be opened, print an error message and return
        printf("Error opening file!\n");
        return;
    }

    // Loop through all student records
    for (int i = 0; i < num_students; i++) {
        // Write each record to the file in CSV format
        // The format string "%s,%s,%s,%s,%.2f\n" specifies the format of each line:
        //   %s for strings (name, date of birth, registration number, and program code)
        //   %.2f for a floating-point number (tuition) with 2 decimal places
        fprintf(fp, "%s,%s,%s,%s,%.2f\n", students[i].name, students[i].dob, students[i].registration, students[i].program_code, students[i].tuition);
    }

    // Close the file
    fclose(fp);

    // Print a success message
    printf("Student records exported successfully!\n");
}